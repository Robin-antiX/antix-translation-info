��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  :   �     "  /   (     X  	   n  :   x     �     �     �     �     	  0   	  b   E	  -   �	  
   �	     �	     �	     
  +   
     >
     \
     d
  %   s
     �
  4   �
  ,   �
  0        G  0   Y  %   �     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:43+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: sw
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Vinginevyo, unaweza kuangalia habari zingine za eneo lako. Tumia Je! Unakutana na maeneo ambayo hayajatafsiriwa? Lugha zinazopatikana: Toa mimba Bonyeza "Endelea" baada ya kuanzisha unganisho la Mtandao. Malizia  Tatizo la mtandao Onyesha habari za eneo lako Je! Unaona Kiingereza tu? Usionyeshe tena Ikiwa unaelewa Kiingereza kidogo pamoja na lugha Ni rahisi kuliko unavyofikiria,
na unaweza kuchangia mengi au kidogo
kama unavyotaka. Jisajili tu. Saidia timu ya kujitolea ya tafsiri ya antiX, Ifanye tu. Habari ya lugha uteuzi wa lugha Taarifa za ziada Hakuna muunganisho wa mtandao unaopatikana. Tafadhali chagua lugha kutoka Endelea Kuchagua lugha Ufikiaji wako kwa timu (bila malipo): Hiyo inaweza kubadilishwa. yako ya mama, unaweza kusaidia kuhakikisha kuwa hivi antiX bado haijatafsiriwa katika lugha yako? karibuni unaweza kupata antiX katika lugha yako. kuona habari hii. Ili kufanya hivyo, bonyeza kitufe kinachofanana. Mnakaribishwa sana kuchangia tafsiri. kwenye menyu ambayo ungependa 