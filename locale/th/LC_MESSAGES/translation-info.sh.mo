��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  W   �  	   ?  ^   I  (   �     �  �   �     �	  B   �	  6   �	  =   3
  *   q
  ~   �
      @   "     c     }  $   �  -   �  W   �  -   G     u  $   �  F   �  E   �     =  g   �  v   %  *   �  �   �  �   P  -   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:49+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 หรือจะดูข้อมูลในท้องถิ่นก็ได้ ใช้ คุณพบพื้นที่ที่ไม่ได้แปลหรือไม่? ภาษาที่ใช้ได้: ยกเลิก คลิกที่ "ดำเนินการต่อ" หลังจากที่คุณสร้างการเชื่อมต่ออินเทอร์เน็ตแล้ว สรุป ข้อผิดพลาดของเครือข่าย แสดงข้อมูลท้องถิ่น คุณเห็นแต่ภาษาอังกฤษ? ไม่ต้องแสดงอีก หากคุณเข้าใจภาษาอังกฤษเพียงเล็กน้อยนอกเหนื ง่ายกว่าที่คุณคิด และคุณสามาร
ถมีส่วนร่วมได้มากหรือน้อยตามที่
คุณต้องการ เพียงแค่ลงทะเบียน ช่วยทีมแปล antiX อาสาสมัคร แค่ทำมัน. ข้อมูลภาษา การเลือกภาษา ข้อมูลเพิ่มเติม ไม่มีการเชื่อมต่ออินเทอร์เน็ต โปรดเลือกภาษาจา ทำต่อไป การเลือกภาษา การเข้าถึงทีมของคุณ (ฟรี): ที่สามารถเปลี่ยนแปลงได้ อจากภาษาแม่ของคุณ คุณสามารถช่วยให้มั่นใจว่า antiX ยังไม่ได้รับการแปลเป็นภาษาของคุณ? คุณจะได้สัมผัสกับ antiX ในภาษาของคุณในไม่ช้า การดูข้อมูลนี้ เมื่อต้องการทำเช่นนี้ ให้คลิกปุ่มที่เกี่ยวข้อง คุณยินดีเป็นอย่างยิ่งที่จะมีส่วนร่วมในการแปล กเมนูที่คุณต้อง 