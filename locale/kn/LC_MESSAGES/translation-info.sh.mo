��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �     s  v   �  2   �  $   *	  �   O	     
  (   %
  G   N
  U   �
  (   �
  �     	  �  {   �  #   #     G     g  .   �  K   �  >     "   A  7   d  T   �  8   �  �   *  �   �  �   n  9     m   ?  x   �  K   &               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:33+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: kn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 ಪರ್ಯಾಯವಾಗಿ, ನೀವು ಕೆಲವು ಸ್ಥಳೀಯ ಮಾಹಿತಿಯನ್ನು ನೋಡಬಹುದು. ಬಳಸಿ ನೀವು ಅನುವಾದಿಸದ ಪ್ರದೇಶಗಳನ್ನು ಎದುರಿಸುತ್ತೀರಾ? ಲಭ್ಯವಿರುವ ಭಾಷೆಗಳು: ಸ್ಥಗಿತಗೊಳಿಸಿ ನೀವು ಇಂಟರ್ನೆಟ್ ಸಂಪರ್ಕವನ್ನು ಸ್ಥಾಪಿಸಿದ ನಂತರ "ಮುಂದುವರಿಸಿ" ಕ್ಲಿಕ್ ಮಾಡಿ. ತೀರ್ಮಾನಿಸಿ  ನೆಟ್‌ವರ್ಕ್ ದೋಷ ಸ್ಥಳೀಯ ಮಾಹಿತಿಯನ್ನು ತೋರಿಸಿ ನೀವು ಇಂಗ್ಲಿಷ್ ಮಾತ್ರ ನೋಡುತ್ತೀರಾ? ಮತ್ತೆ ತೋರಿಸಬೇಡ ನಿಮ್ಮ ಮಾತೃಭಾಷೆಯ ಜೊತೆಗೆ ನೀವು ಸ್ವಲ್ಪ ಇಂಗ್ಲಿಷ್ ಅನ್ನು ನೀವು ಯೋಚಿಸುವುದಕ್ಕಿಂತ ಇದು ಸುಲಭ,
ಮತ್ತು ನೀವು ಎಷ್ಟು ಬೇಕಾದರೂ ಅಥವಾ ಕಡಿಮೆ
ಕೊಡುಗೆ ನೀಡಬಹುದು. ಕೇವಲ ನೋಂದಾಯಿಸಿ. ಸ್ವಯಂಸೇವಕ ಆಂಟಿಎಕ್ಸ್ ಅನುವಾದ ತಂಡಕ್ಕೆ ಸಹಾಯ ಮಾಡಿ, ಸುಮ್ಮನೆ ಮಾಡು. ಭಾಷಾ ಮಾಹಿತಿ ಭಾಷೆಯ ಆಯ್ಕೆ ಹೆಚ್ಚುವರಿ ಮಾಹಿತಿ ಇಂಟರ್ನೆಟ್ ಸಂಪರ್ಕ ಲಭ್ಯವಿಲ್ಲ. ದಯವಿಟ್ಟು ಈ ಮಾಹಿತಿಯನ್ನು ಹೋಗ್ತಾ ಇರು ಭಾಷೆಯನ್ನು ಆರಿಸುವುದು ತಂಡಕ್ಕೆ ನಿಮ್ಮ ಪ್ರವೇಶ (ಉಚಿತವಾಗಿ): ಅದನ್ನು ಬದಲಾಯಿಸಬಹುದು. ಅರ್ಥಮಾಡಿಕೊಂಡರೆ, ನಿಮ್ಮ ಭಾಷೆಯಲ್ಲಿ ಆಂಟಿಎಕ್ಸ್ ಅನ್ನು ನೀವು ಶೀಘ್ರದಲ್ಲೇ ಆಂಟಿಎಕ್ಸ್ ಅನ್ನು ನಿಮ್ಮ ಭಾಷೆಗೆ ಇನ್ನೂ ಅನುವಾದಿಸಲಾಗಿಲ್ಲವೇ? ಅನುಭವಿಸಬಹುದು ಎಂದು ಖಚಿತಪಡಿಸಿಕೊಳ್ಳಲು ನೀವು ಸಹಾಯ ಮಾಡಬಹುದು. ಭಾಷೆಯನ್ನು ಆಯ್ಕೆ ಮಾಡಿ. ಇದನ್ನು ಮಾಡಲು, ಅನುಗುಣವಾದ ಬಟನ್ ಕ್ಲಿಕ್ ಮಾಡಿ. ಅನುವಾದಗಳಿಗೆ ಕೊಡುಗೆ ನೀಡಲು ನಿಮಗೆ ತುಂಬಾ ಸ್ವಾಗತ. ನೀವು ನೋಡಲು ಬಯಸುವ ಮೆನುವಿನಿಂದ 