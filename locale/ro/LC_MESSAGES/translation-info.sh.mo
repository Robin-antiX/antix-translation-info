��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  9   �  	   !  !   +     M  	   `  W   j     �     �     �     �     
	  <   #	     `	  2   �	     
     
     7
     I
  !   _
     �
  	   �
     �
  ,   �
     �
  6     7   =  8   u     �  B   �  3        E               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:33+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Alternativ, puteți vizualiza câteva informații locale. Aplicați Întâlniți secțiuni netraduse? Limbi disponibile: Anulează Faceți clic pe "Continue" (Continuare) după ce ați stabilit o conexiune la internet. Închideți Eroare de rețea Vezi informații locale Vezi doar engleza? Nu se afișează din nou Dacă înțelegeți puțină engleză, pe lângă limba dvs. Este mai ușor decât credeți și
puteți contribui cât de mult sau cât de
puțin doriți. Trebuie doar să vă înscrieți. Ajutați în echipa de traducere voluntară antiX, Doar fă-o. Informații lingvistice Selectarea limbii Mai multe informații Nu există conexiune la internet. Vă rugăm să selectați din Continuă Selectați limba Accesul dumneavoastră la echipă (gratuit): Acest lucru poate fi schimbat. maternă, puteți contribui la asigurarea faptului că antiX nu a fost încă tradus în limba dumneavoastră? în curând veți putea experimenta antiX în limba dvs. să vedeți aceste informații. Pentru a face acest lucru, faceți clic pe butonul corespunzător. Sunteți bineveniți să participați la traduceri. meniu limba în care doriți 