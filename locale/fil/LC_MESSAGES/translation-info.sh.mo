��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  g  �  D   �     -  /   5     e     |  U   �     �     �      �     	     9	  B   N	  �   �	  .   
     C
     T
     h
     x
  *   �
     �
  
   �
     �
  /   �
     &  >   5  /   t  B   �     �  1     8   6     o               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:43+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: fil
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Bilang kahalili, maaari kang tumingin sa ilang lokal na impormasyon. Gamitin Nakatagpo ka ba ng mga hindi naisalin na lugar? Magagamit na mga wika: Pagpapalaglag Mag-click sa "Magpatuloy" pagkatapos mong maitaguyod ang isang koneksyon sa Internet. Tapusin  Error sa network Ipakita ang lokal na impormasyon English lang ang nakikita mo? Huwag magpakita ulit Kung naiintindihan mo ang isang maliit na Ingles bilang karagdagan Ito ay mas madali kaysa sa iniisip mo,
at maaari kang makapag-ambag ng marami o kaunting
hangga't gusto mo. Magparehistro ka lang. Tulungan ang volunteer antiX translation team, Gawin mo nalang. Impormasyon sa wika pagpili ng wika karagdagang impormasyon Walang magagamit na koneksyon sa internet. Mangyaring piliin ang wika mula Tuloy lang Pagpili ng wika Ang iyong pag-access sa koponan (walang bayad): Mababago iyon. sa iyong katutubong wika, makakatulong kang matiyak na maaari  Ang antiX ay hindi pa naisasalin sa iyong wika? mong maranasan sa lalong madaling panahon ang antiX sa iyong wika. makita ang impormasyong ito. Upang magawa ito, i-click ang kaukulang pindutan. Malugod na tinatanggap kang mag-ambag sa mga pagsasalin. sa menu kung saan nais mong 