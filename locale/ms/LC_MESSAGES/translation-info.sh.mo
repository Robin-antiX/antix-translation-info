��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  B   �     *  8   2     k     |  :   �     �     �     �  *    	     +	  6   A	  z   x	  +   �	     
     ,
     <
     M
  +   _
     �
     �
     �
      �
     �
  5   �
  /   #  8   S     �  -   �  <   �                    
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:31+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Sebagai alternatif, anda boleh melihat beberapa maklumat tempatan. Gunakan Adakah anda menghadapi kawasan yang tidak diterjemahkan? Bahasa yang ada: Batalkan Klik »Teruskan« setelah anda membuat sambungan Internet. Membuat kesimpulan  Ralat rangkaian Tunjukkan maklumat tempatan Adakah anda hanya melihat Bahasa Inggeris? Jangan tunjukkan lagi Sekiranya anda memahami sedikit bahasa Inggeris selain Lebih mudah daripada yang anda fikirkan,
dan anda boleh menyumbang sebanyak atau
sedikit yang anda mahukan. Daftar sahaja. Bantu pasukan terjemahan antiX sukarelawan, Buat sahaja. Maklumat bahasa pemilihan bahasa maklumat tambahan Tidak ada sambungan internet yang tersedia. Sila pilih bahasa dari Teruskan Memilih bahasa Akses anda ke pasukan (percuma): Itu boleh diubah. bahasa ibunda anda, anda boleh memastikan bahawa anda antiX belum diterjemahkan ke dalam bahasa anda? tidak lama lagi boleh mengalami antiX dalam bahasa anda. melihat maklumat ini. Untuk melakukan ini, klik butang yang sesuai. Anda sangat dialu-alukan untuk menyumbang kepada terjemahan. menu di mana anda ingin 