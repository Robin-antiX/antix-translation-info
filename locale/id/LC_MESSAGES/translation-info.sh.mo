��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  4   �       4   (     ]     s  7   �     �     �     �  )   �     	  8   3	  �   l	  '   �	     
     %
     6
     E
  )   X
     �
     �
     �
     �
     �
  3   �
  /     /   L     |  -   �  6   �     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:28+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Atau, Anda dapat memeriksa beberapa informasi lokal. Menggunakan Apakah Anda menemukan area yang tidak diterjemahkan? Bahasa yang tersedia: Menggugurkan Klik "Lanjutkan" setelah Anda membuat koneksi Internet. Menyimpulkan  Kesalahan jaringan Tampilkan informasi lokal Apakah Anda hanya melihat bahasa Inggris? Jangan tampilkan lagi Jika Anda memahami sedikit bahasa Inggris selain bahasa  Ini lebih mudah daripada yang Anda pikirkan,
dan Anda dapat berkontribusi sebanyak atau
sesedikit yang Anda inginkan. Daftar saja. Bantu tim penerjemah antiX sukarelawan, Lakukan saja. Informasi bahasa pilihan bahasa informasi tambahan Tidak ada koneksi internet yang tersedia. Silakan pilih bahasa dari Terus berlanjut Memilih bahasa Akses Anda ke tim (gratis): Itu bisa diubah. ibu Anda, Anda dapat membantu memastikan bahwa Anda antiX belum diterjemahkan ke dalam bahasa Anda? dapat segera mengalami antiX dalam bahasa Anda. melihat informasi ini. Untuk melakukan ini, klik tombol yang sesuai. Anda dipersilakan untuk berkontribusi pada terjemahan. menu di mana Anda ingin 