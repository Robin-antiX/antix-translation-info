��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �  !   �  �   �  &   :	  $   a	  �   �	     R
  7   f
  P   �
  g   �
  7   W  �   �  B  A  �   �     *  %   H  1   n     �  N   �  ?     N   L  =   �  x   �  8   R  �   �  �   5  �   �  5   }  �   �  �   ;  P   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:31+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 പകരമായി, നിങ്ങൾക്ക് ചില പ്രാദേശിക വിവരങ്ങൾ പരിശോധിക്കാം. ഉപയോഗിക്കുക നിങ്ങൾ വിവർത്തനം ചെയ്യാത്ത പ്രദേശങ്ങൾ നേരിടുന്നുണ്ടോ? ലഭ്യമായ ഭാഷകൾ: ഉപേക്ഷിക്കുക നിങ്ങൾ ഒരു ഇന്റർനെറ്റ് കണക്ഷൻ സ്ഥാപിച്ചതിന് ശേഷം "തുടരുക" ക്ലിക്ക് ചെയ്യുക. നിഗമനം  നെറ്റ്‌വർക്ക് പിശക് പ്രാദേശിക വിവരങ്ങൾ കാണിക്കുക നിങ്ങൾക്ക് ഇംഗ്ലീഷ് മാത്രമേ കാണാനാകൂ? വീണ്ടും കാണിക്കരുത് നിങ്ങളുടെ മാതൃഭാഷയ്‌ക്ക് പുറമേ അൽപ്പം ഇംഗ്ലീഷ് മനസ്സിലാക്കിയാൽ, നിങ്ങൾ വിചാരിക്കുന്നതിനേക്കാൾ എളുപ്പമാണ്,
നിങ്ങൾ ആഗ്രഹിക്കുന്നത്രയും കുറച്ചും
സംഭാവന ചെയ്യാം. രജിസ്റ്റർ ചെയ്താൽ മതി. സന്നദ്ധപ്രവർത്തകനായ ആന്റി എക്സ് വിവർത്തന ടീമിനെ സഹായിക്കുക, ഇത് ചെയ്യൂ. ഭാഷാ വിവരങ്ങൾ ഭാഷ തിരഞ്ഞെടുക്കൽ അധിക വിവരം ഇന്റർനെറ്റ് കണക്ഷൻ ലഭ്യമല്ല. നിങ്ങൾ ഈ വിവരങ്ങൾ കാണാൻ പൊയ്ക്കൊണ്ടേയിരിക്കുന്നു ഭാഷ തിരഞ്ഞെടുക്കുന്നു ടീമിലേക്കുള്ള നിങ്ങളുടെ ആക്സസ് (സൗജന്യമായി): അത് മാറ്റാവുന്നതാണ്. നിങ്ങൾക്ക് ഉടൻ തന്നെ നിങ്ങളുടെ ഭാഷയിൽ ആന്റി എക്സ് അനുഭവിക്കാൻ ആന്റി എക്സ് ഇതുവരെ നിങ്ങളുടെ ഭാഷയിലേക്ക് വിവർത്തനം ചെയ്തിട്ടില്ലേ? കഴിയുമെന്ന് ഉറപ്പാക്കാൻ നിങ്ങൾക്ക് സഹായിക്കാനാകും. ഭാഷ തിരഞ്ഞെടുക്കുക. ഇത് ചെയ്യുന്നതിന്, അനുബന്ധ ബട്ടൺ ക്ലിക്കുചെയ്യുക. വിവർത്തനങ്ങളിലേക്ക് സംഭാവന ചെയ്യാൻ നിങ്ങളെ വളരെ സ്വാഗതം ചെയ്യുന്നു. ആഗ്രഹിക്കുന്ന മെനുവിൽ നിന്ന് 