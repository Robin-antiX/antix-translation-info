��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  �  �  3   8     l  :   t     �     �  >   �     	  
   	     	  W   4	     �	  (   �	  r   �	  -   ;
     i
     u
     �
     �
     �
     �
     �
     �
  '   �
       0   !  (   R  -   {     �  !   �      �     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-22 23:18+0200
Last-Translator: Robin, 2021
Language-Team: Estonian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/et/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: et
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Teise võimalusena võite vaadata kohalikku teavet. Rakenda Kas te puutute kokku valdkondadega, mida ei ole tõlgitud? Saadaolevad keeled: Tühista Pärast internetiühenduse loomist klõpsake nuppu »Jätka«. Sulge Võrguviga Näita kohalikku teavet Sa näed ainult                              
  inglise keelt?                          Ära näita uuesti Kui sa mõistad lisaks oma emakeelele ka See on lihtsam, kui te arvate, ja te võite panustada
    nii palju või vähe, kui soovite. Lihtsalt registreeru. Abi vabatahtlike antiX tõlkijate meeskonnas, Just do it. Häälteave Keele valik Täiendav teave Internetiühendus puudub. Valige menüüst keel, milles Jätka Valige keel Teie juurdepääs meeskonnale (tasuta): Seda saab muuta. veidi inglise keelt, võid aidata kaasa sellele, antiX ei ole veel teie keelde tõlgitud? et sa saaksid peagi kogeda antiXi oma keeles.   Selleks klõpsake vastaval nupul. Olete oodatud osalema tõlgetes. soovite seda teavet näha. 