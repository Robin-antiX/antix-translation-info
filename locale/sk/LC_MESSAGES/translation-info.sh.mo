��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  =   �  
   %  +   0     \     n  M   w  	   �     �     �     �     	  1   &	  |   X	  :   �	     
     &
     <
     J
  ,   \
     �
     �
     �
  #   �
     �
  5   �
  0   )  9   Z     �  9   �  +   �                    
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:33+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Prípadne si môžete pozrieť niektoré miestne informácie. Aplikovať Stretávate sa s nepreloženými časťami? Dostupné jazyky: Zrušiť Po vytvorení internetového pripojenia kliknite na tlačidlo "Pokračovať". Zatvoriť Chyba siete Zobraziť miestne informácie Vidíte iba angličtinu? Nezobrazovať znova Ak okrem svojho materinského jazyka rozumiete aj Je to jednoduchšie, ako si myslíte,
a môžete prispieť tak veľa alebo tak
málo, ako chcete. Stačí sa zaregistrovať. Pomôžte v dobrovoľníckom prekladateľskom tíme antiX, Jednoducho to urobte. Jazykové informácie Výber jazyka Viac informácií Nie je k dispozícii pripojenie na internet. V ponuke vyberte jazyk, Pokračovať Výber jazyka Váš prístup k tímu (bezplatne): To sa dá zmeniť. trochu angličtine, môžete prispieť k tomu, že si antiX ešte nebol preložený do vášho jazyka? čoskoro budete môcť vychutnať antiX vo svojom jazyku. informácie zobraziť. Ak to chcete urobiť, kliknite na príslušné tlačidlo. Na prekladoch sa môžete podieľať aj vy. v ktorom chcete tieto 