��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  c   �     K  G   c     �     �  \   �  	   3	     =	  "   O	  6   r	     �	  X   �	  �   
  <   �
          )     A     W  7   o  )   �     �     �  /   �  ,   )  [   V  @   �  4   �  '   (  T   P  K   �  3   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:26+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 متناوبا ، می توانید برخی از اطلاعات محلی را بررسی کنید. استفاده کنید آیا با مناطق ترجمه نشده برخورد می کنید؟ زبانهای موجود: سقط کردن پس از برقراری اتصال اینترنت ، روی "ادامه" کلیک کنید. بستن  خطای شبکه نمایش اطلاعات محلی  آیا شما فقط انگلیسی می بینید؟ دیگه نشون نده اگر علاوه بر زبان مادری خود کمی انگلیسی می دانید  این آسان تر از آن است که فکر می کنید ،
و می توانید هر چقدر که می خواهید
یا کم کمک کنید. فقط ثبت نام کنید به تیم ترجمه داوطلب antiX کمک کنید ، فقط انجامش بده اطلاعات زبان انتخاب زبان اطلاعات دیگر اتصال به اینترنت در دسترس نیست لطفاً از منویی که مایل  ادامه بده انتخاب زبان دسترسی شما به تیم (رایگان): این را می توان تغییر داد. ، می توانید اطمینان حاصل کنید که به زودی می توانید  هنوز antiX به زبان شما ترجمه نشده است؟ antiX را در زبان خود تجربه کنید. ، زبان را انتخاب کنید. برای انجام این کار ، روی دکمه مربوطه کلیک کنید. از مشارکت شما در ترجمه ها بسیار خوش آمدید. به مشاهده این اطلاعات هستید  