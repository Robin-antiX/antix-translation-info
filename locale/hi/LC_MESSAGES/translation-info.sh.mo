��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �     s  q   �      �  *   	  �   A	  /   �	  (   
  >   =
  T   |
  )   �
  �   �
  �   �  p        �  )        2  .   I  _   x  7   �          $  I   B  4   �  �   �  �   L  �   �  ,   `  o   �  �   �  F   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:27+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 वैकल्पिक रूप से, आप कुछ स्थानीय जानकारी देख सकते हैं। उपयोग क्या आप अनूदित क्षेत्रों का सामना करते हैं? उपलब्ध भाषा: बीच में बंद करें इंटरनेट कनेक्शन स्थापित करने के बाद "जारी रखें" पर क्लिक करें। निष्कर्ष निकालना  नेटवर्क त्रुटि स्थानीय जानकारी दिखाएं क्या आप केवल अंग्रेजी देखते हैं? दोबारा मत दिखाओ यदि आप अपनी मातृभाषा के अलावा थोड़ी सी अंग्रेजी भी यह आपके विचार से आसान है, और आप जितना
 चाहें उतना कम या ज्यादा योगदान
 कर सकते हैं। बस रजिस्टर करें। स्वयंसेवक एंटीएक्स अनुवाद टीम की मदद करें, बस कर दो। भाषा की जानकारी भाषा चयन अतिरिक्त जानकारी कोई इंटरनेट कनेक्शन उपलब्ध नहीं है। कृपया उस मेनू से भाषा बढ़ा चल भाषा का चयन टीम तक आपकी पहुंच (निःशुल्क): इसे बदला जा सकता है। समझते हैं, तो आप यह सुनिश्चित करने में मदद कर सकते हैं एंटीएक्स का अभी तक आपकी भाषा में अनुवाद नहीं हुआ है? कि आप जल्द ही अपनी भाषा में एंटीएक्स का अनुभव कर सकें। देखना चाहते हैं। ऐसा करने के लिए, संबंधित बटन पर क्लिक करें। अनुवाद में योगदान देने के लिए आपका बहुत-बहुत स्वागत है। चुनें जिसमें आप यह जानकारी 