��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  >   �     &  $   .     S     g  I   n  	   �  	   �     �     �      	  5   	  {   G	  7   �	     �	     
     (
     6
     M
     h
     �
     �
  .   �
     �
  0   �
  6     .   R     �  2   �  I   �  !                  
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:45+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Að öðrum kosti geturðu skoðað staðbundnar upplýsingar. Notaðu Lendir þú í óþrýddum svæðum? Tungumál í boði: Hætta Smelltu á „Halda áfram“ eftir að þú hefur komið á nettengingu. Ályktun  Net villa Sýna staðbundnar upplýsingar Sérðu bara ensku? Ekki sýna aftur Ef þú skilur smá ensku til viðbótar móðurmáli Það er auðveldara en þú heldur og þú
getur lagt fram eins mikið eða eins
lítið og þú vilt. Skráðu þig bara. Hjálpaðu sjálfboðaliði antiX þýðingarteymisins, Gerðu það bara. Upplýsingar um tungumál tungumálaval Viðbótarupplýsingar Engin nettenging í boði. Vinsamlegast veldu tungumálið Haltu áfram Að velja tungumál Aðgangur þinn að liðinu (án endurgjalds): Því má breyta. þínu geturðu hjálpað til við að þú munt antiX hefur ekki enn verið þýtt á þitt tungumál? fljótlega upplifa antiX á þínu tungumáli. sjá þessar upplýsingar. Til að gera þetta, smelltu á samsvarandi hnapp. Þér er mjög velkomið að leggja þitt af mörkum við þýðingarnar. í valmyndinni þar sem þú vilt 