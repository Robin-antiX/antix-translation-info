��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  L   �     4  F   @     �  
   �  Q   �     �     	  '   	     8	     X	  9   r	  p   �	  (   
     F
     U
     l
     �
      �
     �
     �
     �
  8   �
  ,   0  E   ]  ?   �  ;   �       =   4  ;   r      �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:39+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Ngoài ra, bạn có thể kiểm tra một số thông tin địa phương. Sử dụng Bạn có gặp phải những khu vực chưa được dịch không? Ngon ngu co san: Huỷ bỏ Nhấp vào "Tiếp tục" sau khi bạn đã thiết lập kết nối Internet. Kết luận  Lỗi mạng Hiển thị thông tin địa phương Bạn chỉ thấy tiếng Anh? Không hiển thị lại Nếu bạn hiểu một chút tiếng Anh ngoài tiếng Nó dễ dàng hơn bạn nghĩ và bạn
có thể đóng góp nhiều hay ít
tùy ý. Chỉ cần đăng ký. Giúp nhóm dịch antiX tình nguyện, Cứ làm đi. Thông tin ngôn ngữ sự lựa chọn ngôn ngữ thông tin thêm Không có kết nối internet. Vui lòng chọn ngôn Tiếp tục đi Chọn ngôn ngữ Quyền truy cập của bạn vào nhóm (miễn phí): Điều đó có thể được thay đổi. mẹ đẻ, bạn có thể giúp đảm bảo rằng bạn có thể antiX vẫn chưa được dịch sang ngôn ngữ của bạn? sớm trải nghiệm antiX bằng ngôn ngữ của mình. xem thông tin này. Để làm điều này, hãy nhấp vào nút tương ứng. Rất hoan nghênh bạn đóng góp cho các bản dịch. ngữ từ menu mà bạn muốn 