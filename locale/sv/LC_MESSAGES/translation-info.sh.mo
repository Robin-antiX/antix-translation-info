��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  -   �  
     #         D     Z  D   a     �     �     �     �     �  %   �  r   	  9   �	     �	     �	     �	     �	  -   
     :
  	   L
     V
  *   c
     �
  %   �
  1   �
  +   �
     %  0   6  1   g     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:36+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Alternativt kan du se lite lokal information. Ansök på Stöter du på oöversatta avsnitt? Tillgängliga språk: Avbryt Klicka på "Fortsätt" när du har upprättat en internetanslutning. Stäng Nätverksfel Visa lokal information Ser du bara engelska? Visa inte igen Om du förstår lite engelska utöver Det är lättare än du tror och du kan
bidra så mycket eller så lite som du
vill. Det är bara att anmäla sig. Hjälp till i det frivilliga antiX-översättningsteamet, Gör det bara. Information om språk Val av språk Mer information Ingen internetuppkoppling finns tillgänglig. Välj det du vill Fortsätt Välj språk Din tillgång till teamet (kostnadsfritt): Detta kan ändras. ditt modersmål kan du bidra till att antiX har ännu inte översatts till ditt språk? du snart kan uppleva antiX på ditt språk. på från menyn. Klicka på motsvarande knapp för att göra det. Du är välkommen att delta i översättningarna. se informationen 